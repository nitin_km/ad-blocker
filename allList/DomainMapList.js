var DomainElementList = new Map([["www.youtube.com", [
        // "promoted-sparkles-text-search-root-container", "promoted-videos", "searchView.list-view",
        // "watch-extra-info-column", "watch-extra-info-right", "ytd-compact-promoted-video-renderer",
        // "ytd-merch-shelf-renderer", "ytd-promoted-sparkles-text-search-renderer", "ytd-search-pyv-renderer",
        // "ytd-compact-promoted-video-renderer", "ytd-promoted-sparkles-text-search-renderer", "feed-pyv-container",
        // "feedmodule-PRO", "homepage-chrome-side-promo", "merch-shelf", "pla-shelf", "premium-yva", "promo-info",
        // "promo-list", "search-pva", "shelf-pyv-container", "video-masthead", "watch-branded-actions", "watch-buy-urls", "watch-channel-brand-div",
        "header-top", "ytd-promoted-sparkles-web-renderer", "ytd-promoted-sparkles-text-search-renderer", "YTM-PROMOTED-VIDEO-RENDERER",
        "ytd-banner-promo-renderer-background","ytd-video-masthead-ad-advertiser-info-renderer", "ytp-ad-overlay-close-container", "ytp-ad-overlay-image",
        "ytd-video-masthead-ad-v3-renderer",
        "ytd-action-companion-ad-renderer",
        "ytd-image-companion-renderer", "ytd-promoted-video-renderer",
        "ytd-compact-promoted-video-renderer"
    ]],
        ["www.youtubelike.com", ["bottom-thumbs", "bottom-top", "gallery-thumbs"]]]
);