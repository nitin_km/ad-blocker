let storageKeys = {};
storageKeys = {
    optedAdBlock: "optedAdBlock",
    optedAdTrack: "optedAdTrack",
    optedRiskySites: "optedRiskySites",
    adBlockCount: "adBlockCount",
    adTrackCount: "adTrackCount",
    riskySitesCount: "riskySitesCount",
    whiteListedDomain: "whiteListedDomain",
    nonIntrusiveAdsOptin: "nonIntrusiveAdsOptin",
    setTabActive: "setTabActive",
};

let deletionTime = 1,
    whiteListedDomain = [];

var storePauseAllAds = false;

function updatePopup() {
    const localDomain = localStorage.getItem(storageKeys.whiteListedDomain);
    whiteListedDomain = localDomain ? JSON.parse(localDomain) : [];
    updateWhiteWebsites(whiteListedDomain);

    const adBlockStatus = localStorage.getItem(storageKeys.optedAdBlock);
    const adBlockButton = document.querySelector('input[name=blockAdd]');
    if (adBlockStatus === 'yes') {
        adBlockButton.checked = true;
    } else {
        adBlockButton.checked = false;
    }

    const pauseAdBlockingAllSitesStatus = localStorage.getItem(storageKeys.optedAdBlock);
    const pauseAdBlockingAllSitesStatusButton = document.querySelector('input[name=blockallsite]');
    if (pauseAdBlockingAllSitesStatus === 'no') {
        pauseAdBlockingAllSitesStatusButton.checked = true;
    } else {
        pauseAdBlockingAllSitesStatusButton.checked = false;
    }

    const nonIntrusiveAdStatus = localStorage.getItem(storageKeys.nonIntrusiveAdsOptin);
    const nonIntrusiveAdButton = document.querySelector('input[name=allowIntrusiveAds]');
    if (nonIntrusiveAdStatus === 'yes') {
        nonIntrusiveAdButton.checked = true;
    } else {
        nonIntrusiveAdButton.checked = false;
    }

    const adTrackStatus = localStorage.getItem(storageKeys.optedAdTrack);
    const adTrackButton = document.querySelector('input[name=blockTracker]');
    if (adTrackStatus === 'yes') {
        adTrackButton.checked = true;
    } else {
        adTrackButton.checked = false;
    }

    const riskySitesStatus = localStorage.getItem(storageKeys.optedRiskySites);
    const riskySitesButton = document.querySelector('input[name=blockRiskySites]');
    if (riskySitesStatus === 'yes') {
        riskySitesButton.checked = true;
    } else {
        riskySitesButton.checked = false;
    }

    const adBlockCount = localStorage.getItem(storageKeys.adBlockCount);
    const adBlockReportCount = document.getElementById('adBlockCount');
    adBlockReportCount.innerHTML = adBlockCount;

    const adTrackCount = localStorage.getItem(storageKeys.adTrackCount);
    const adTrackReportCount = document.getElementById('adTrackCount');
    adTrackReportCount.innerHTML = adTrackCount;

    const riskySitesCount = localStorage.getItem(storageKeys.riskySitesCount);
    const riskySitesReportCount = document.getElementById('riskySitesCount');
    riskySitesReportCount.innerHTML = riskySitesCount;

    const checkBoxes = document.querySelectorAll('.checkbox');
    Array.from(checkBoxes).forEach(function (e) {
        e.style.display = 'inline-block';
    });

    var localTabData = localStorage.getItem(storageKeys.setTabActive);
    if (localTabData && !isEmptyObj(JSON.parse(localTabData))) {
        var parsedTemp = JSON.parse(localTabData);
        for (var key in parsedTemp) {
            if (parsedTemp.hasOwnProperty(key) && parsedTemp[key]) {
                document.querySelector('#' + key).classList.add('active');
                document
                    .querySelector('[data-rel=' + key + ']')
                    .classList.add('active');
            }
        }
    }
}

window.addEventListener('DOMContentLoaded', function () {
    updatePopup();
});

window.addEventListener('storage', function (e) {
    if (e.key == 'optedAdBlock') updatePopup();
});

chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
    if (whiteListedDomain == null) {
        whiteListedDomain = [];
    }
    let setCurrentTabUrlStatus = false;
    chrome.runtime.sendMessage(
        {domainUrl: tabs[0].url, type: 'extractDomainUrl'},
        function (response) {
            const index = whiteListedDomain.indexOf(response.properUrl);
            if (index > -1) {
                setCurrentTabUrlStatus = true;
            }
            const pauseAdForCurrentTab = document.querySelector('input[name=blockthissite]');
            if (setCurrentTabUrlStatus) {
                pauseAdForCurrentTab.checked = true;
            } else {
                pauseAdForCurrentTab.checked = false;
            }
        }
    );

})

const cancelButton = document.querySelector('.cancelButton');
cancelButton.addEventListener('click', onCancelClick);

function onCancelClick() {
    for (let i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = false;
    }
    deleteButton.classList.remove('active');
    const dropDown = document.getElementById('clearDataDuration');
    dropDown.selectedIndex = 0;
}

const deleteButton = document.querySelector('.clearButton');
deleteButton.addEventListener('click', deleteBrowsingData);

function sendStatusToBackground(msg) {
    localStorage.setItem(storageKeys.nonIntrusiveAdsOptin, msg);
    chrome.runtime.sendMessage(
        {[storageKeys.nonIntrusiveAdsOptin]: msg, type: 'uiToBackground'},
        function (response) {
            console.log('Response: ', response);
        }
    );
}

const adBlockButton = document.querySelector('input[name=blockAdd]');
adBlockButton.addEventListener('change', function (e) {
    if (e.currentTarget.checked) {
        localStorage.setItem(storageKeys.optedAdBlock, 'yes');
        changePauseStatus(true);
    } else {
        localStorage.setItem(storageKeys.optedAdBlock, 'no');
        changePauseStatus(false);
        chrome.runtime.sendMessage({type: 'resetAdCountOfEachTab'},
            function (response) {
                console.log('Response: ', response);
            }
        );
    }
});

const adTrackButton = document.querySelector('input[name=blockTracker]');
adTrackButton.addEventListener('change', function (e) {
    if (e.currentTarget.checked) {
        localStorage.setItem(storageKeys.optedAdTrack, 'yes');
    } else {
        localStorage.setItem(storageKeys.optedAdTrack, 'no');
    }
});

const nonIntrusiveAdsButton = document.querySelector('input[name=allowIntrusiveAds]');
nonIntrusiveAdsButton.addEventListener('change', function (e) {
    if (e.currentTarget.checked) {
        sendStatusToBackground('yes');
    } else {
        sendStatusToBackground('no');
    }
});

const riskySitesButton = document.querySelector('input[name=blockRiskySites]');
riskySitesButton.addEventListener('change', function (e) {
    if (e.currentTarget.checked) {
        localStorage.setItem(storageKeys.optedRiskySites, 'yes');
    } else {
        localStorage.setItem(storageKeys.optedRiskySites, 'no');
    }
});

const addCurrentTabDomain = document.querySelector('input[name=blockthissite]');
addCurrentTabDomain.addEventListener('change', function (e) {
    if (e.currentTarget.checked) {
        chrome.runtime.sendMessage({type: 'whiteListCurrentTabDomain'},
            function (response) {
                console.log('Response: ', response);
            });
    } else {
        chrome.runtime.sendMessage({type: 'blackListCurrentTabDomain'},
            function (response) {
                console.log('Response: ', response);
            });
    }
});

const timeDuration = document.getElementById('clearDataDuration');
timeDuration.addEventListener('change', function () {
    deletionTime = parseInt(this.value);
});

const blockAllSite = document.querySelector('input[name=blockallsite]');
var adBlockBtn = document.querySelector('input[name=blockAdd]');

blockAllSite.addEventListener('change', function (e) {
    if (e.currentTarget.checked) {
        //storePauseAllAds = true;
        localStorage.setItem(storageKeys.optedAdBlock, 'no');
        changeBlockStatus(false);
    } else {
        storePauseAllAds = false;
        localStorage.setItem(storageKeys.optedAdBlock, 'yes');
        changeBlockStatus(true);
    }
});

function changePauseStatus(_arg) {
    if (_arg) {
        blockAllSite.checked = false;
    } else {
        blockAllSite.checked = true;
    }
}

function changeBlockStatus(_arg) {
    if (_arg) {
        adBlockBtn.checked = true;
    } else {
        adBlockBtn.checked = false;
    }
}

function deleteBrowsingData() {
    var checkboxes = document.querySelectorAll('input[name="check"]:checked');
    if (checkboxes.length > 0) {
        var deletepopup = document.querySelector('.deletepopup');
        deletepopup.classList.add('active');
        setTimeout(function () {
            deletepopup.classList.remove('active');
            var tab1 = document.querySelector('[data-rel="tab1"]');
            tab1.classList.remove('active');
            document.querySelector('#tab1').classList.remove('active');
        }, 2000);

        var JsonObej = {};
        var callback = function () {
        };
        var deleteDuration =
            new Date().getTime() - deletionTime * 60 * 60 * 1000;
        if (deletionTime === 0) deleteDuration = 0;
        Array.prototype.forEach.call(checkboxes, function (el) {
            let dataType = el.value + '';
            JsonObej[dataType] = true;
            if (el.value === 'cache') {
                JsonObej['appcache'] = true;
                JsonObej['cacheStorage'] = true;
            }
        });
        chrome.browsingData.remove(
            {
                since: deleteDuration
            },
            JsonObej,
            callback
        );
        onCancelClick();
    }
}

var checkboxes = document.querySelectorAll('input[name=check]');

var tabtitles = document.querySelectorAll('.ss-list_header');

for (var i = 0; i < tabtitles.length; i++) {
    tabtitles[i].addEventListener('click', function (e) {
        var tabName = e.currentTarget.children[0]
            ? e.currentTarget.children[0].textContent.replace(/\s+/g, '')
            : null;
        if (
            e.currentTarget.className &&
            e.currentTarget.className.indexOf('active') !== -1
        ) {
            console.log({
                event_name: 'UserClick',
                event_action: 'TabClose',
                event_str_value: tabName,
                value: tabName
            });
        } else {
            console.log({
                event_name: 'UserClick',
                event_action: 'TabOpen',
                event_str_value: tabName,
                value: tabName
            });
        }
        var activeTab = e.target.parentNode.getAttribute('data-rel');
        e.currentTarget.classList.toggle('active');
        document.querySelector('#' + activeTab).classList.toggle('active');
        if (activeTab === 'tab3')
            window.scrollTo(0, document.body.scrollHeight);
        var tab1 = document.querySelector('#tab1');
        if (!tab1.classList.contains('active')) {
            onCancelClick();
            deleteButton.classList.remove('active');
            var dropDown = document.getElementById('clearDataDuration');
            dropDown.selectedIndex = 0;
        }

        if (activeTab !== 'tab1') {
            var localTabData = localStorage.getItem(
                storageKeys.setTabActive
            );
            var setLocal = {};
            if (localTabData && !isEmptyObj(JSON.parse(localTabData))) {
                setLocal = JSON.parse(localTabData);
                console.log(setLocal);
                if (setLocal[activeTab]) {
                    setLocal[activeTab] = false;
                } else {
                    setLocal[activeTab] = true;
                }
            } else {
                setLocal[activeTab] = true;
            }
            localStorage.setItem(
                storageKeys.setTabActive,
                JSON.stringify(setLocal)
            );
        }
    });
}


function isEmptyObj(_obj) {
    if (_obj) {
        for (var key in _obj) {
            if (_obj.hasOwnProperty(key)) return false;
        }
        return JSON.stringify(_obj) === JSON.stringify({});
    }
    return true;
}

for (var i = 0; i < checkboxes.length; i++) {
    checkboxes[i].addEventListener('change', toggleDelete);
}

function toggleDelete() {
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked == true) {
            deleteButton.classList.add('active');
            return;
        } else {
            deleteButton.classList.remove('active');
        }
    }
}

var infoBlock = document.querySelectorAll('.opt-info');
if (infoBlock) {
    for (var i = 0; i < infoBlock.length; i++) {
        infoBlock[i].addEventListener('click', tooltipHandle);
    }
}

function tooltipHandle(e) {
    var targetEl = e.currentTarget;
    var tooltipBlock = document.querySelector('.ss-tooltip--block');

    var infoType = e.currentTarget.getAttribute('data-info');
    var existAttr = tooltipBlock.getAttribute('data-openfor');

    if (existAttr && existAttr === infoType) {
        tooltipBlock.setAttribute('data-openfor', '');
        tooltipBlock.style.display = 'none';
    } else {
        tooltipBlock.setAttribute('data-openfor', infoType);
        tooltipBlock.style.display = 'block';
        tooltipBlock.style.top = getOffsetTopLeft(targetEl, true) + 14 + 'px';
        var tooltipText = document.querySelector('.ss-tooltip--text');
        switch (infoType) {
            case 'nonintrusive':
                tooltipText.textContent =
                    'These ads do not interrupt your browsing experience or interfere with the content you view online.';
                tooltipBlock.style.left = '15px';
                document.querySelector('.ss-tt--arrow').style.right = '25%';
                break;
            case 'whlistsite':
                tooltipText.textContent =
                    'You’ve chosen to allow ads to be displayed on the following websites that you trust.';
                tooltipBlock.style.left = '0px';
                document.querySelector('.ss-tt--arrow').style.right = '38%';
                break;
            default:
                break;
        }
    }
}

function getOffsetTopLeft(element, XorY) {
    var offsetTemp = 0;
    while (element && element.className.indexOf('ss-container_inner') === -1) {
        if (XorY) offsetTemp += element.offsetTop;
        else offsetTemp += element.offsetLeft;
        element = element.offsetParent;
    }
    return offsetTemp;
}

var closeTooltip = document.querySelector('.ss-tooltip--close');
if (closeTooltip) {
    closeTooltip.addEventListener('click', handleCloseTooltip);
}

function handleCloseTooltip() {
    var tooltipBlock = document.querySelector('.ss-tooltip--block');
    tooltipBlock.setAttribute('data-openfor', '');
    tooltipBlock.style.display = 'none';
}

document.addEventListener('click', function (e) {
    var tooltipBlock = document.querySelector('.ss-tooltip--box');
    var infoBlock = document.querySelectorAll('.opt-info');
    if (!tooltipBlock.contains(e.target)) {
        var _flag = false;
        for (var i = 0; i < infoBlock.length; i++) {
            if (infoBlock[i].contains(e.target)) {
                _flag = true;
            }
        }
        if (!_flag) handleCloseTooltip();
    }
});

const addDomainButton = document.querySelector('.addDomainButton');
addDomainButton.addEventListener('click', addDomain);

function addDomain() {
    var urlDomain = document.getElementById('domainUrl');
    var inputVal = urlDomain.value.trim();
    if (inputVal) {
        if (whiteListedDomain && whiteListedDomain.indexOf(inputVal) != -1) {
            return;
        }
        whiteListedDomain.push(inputVal);
        updateWhiteWebsites(whiteListedDomain);
        urlDomain.value = '';
        chrome.runtime.sendMessage(
            {domainUrl: inputVal, type: 'whiteListDomain'},
            function (response) {
                console.log('Response: ', response);
            }
        );
    }
}

const addDomainInput = document.getElementById('domainUrl');
addDomainInput.addEventListener('input', function (e) {
    var re = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
    if (re.test(e.currentTarget.value)) {
        addDomainButton.classList.remove('disabled');
    } else {
        addDomainButton.className = addDomainButton.className.includes(
            'disabled'
        )
            ? addDomainButton.className
            : addDomainButton.className + ' disabled';
    }
});

function deleteDomain(el) {
    var urlDomain = el.previousElementSibling.textContent;
    if (urlDomain) {
        whiteListedDomain = whiteListedDomain.filter(function (elm) {
            return elm !== urlDomain;
        });
        updateWhiteWebsites(whiteListedDomain);
        chrome.runtime.sendMessage(
            {domainUrl: urlDomain, type: 'deleteDomain'},
            function (response) {
                console.log('Response: ', response);
            }
        );
    }
}

var deleteParent = document.querySelector('.wh-site_list');
deleteParent.addEventListener('click', function (e) {
    var _path = e.path,
        _flag = false,
        _el;
    if (e.target.matches('.deleteDomainButton')) {
        deleteDomain(e.target);
    } else {
        if (_path.length > 0) {
            for (var i = 0; i < _path.length; i++) {
                if (
                    _path[i] &&
                    !!_path[i].className &&
                    typeof _path[i].className == 'string' &&
                    _path[i].className.indexOf('deleteDomainButton') !== -1
                ) {
                    _el = _path[i];
                    _flag = true;
                }
            }
        }
        if (_flag) {
            deleteDomain(_el);
        }
    }
});

function updateWhiteWebsites(arr) {
    var appendHtmlStr = '';
    if (!!arr) {
        for (var i = 0; i < arr.length; i++) {
            appendHtmlStr += `<li class="wh-site_list--menber">
                    <div class="wh-site_list--title">${arr[i]}</div>
                    <div class="wh-site_list--delete deleteDomainButton">
                        <svg xmlns="http://www.w3.org/2000/svg" width="11" height="12"
                            viewBox="0 0 11 12">
                            <g fill="#5CB6FD">
                                <path
                                    d="M9.63 2.076v8.928c0 .536-.443.978-.98.978H1.947c-.537 0-.979-.436-.979-.978L.961 2.076C.572 2.076 0 2.17 0 1.692c0-.212.171-.383.377-.383H3.32C3.125.655 3.627 0 4.305 0h1.74c.672 0 1.173.655.984 1.31h3.184c.507 0 .507.766 0 .766H9.63zM6.044.76h-1.74c-.347 0-.347.53 0 .53h1.74c.348 0 .348-.53 0-.53zM1.722 2.076l.006 8.928c0 .117.1.218.218.218h6.705c.118 0 .212-.1.212-.218V2.076H1.722z" />
                                <path
                                    d="M4.912 3.632c0-.5.767-.5.767 0v6.458c0 .507-.767.507-.767 0V3.632zM3.178 3.632c0-.5.761-.5.761 0v6.458c0 .507-.76.507-.76 0V3.632zM6.652 3.632c0-.5.76-.5.76 0v6.458c0 .507-.76.507-.76 0V3.632z" />
                            </g>
                        </svg>
                    </div>
                </li>`;
        }
    }
    var appendUl = document.querySelector('.wh-site_list');
    var targetBox = document.querySelector('.wh-site_list--block');

    if (appendHtmlStr) {
        targetBox.classList.remove('empty__list');
    } else {
        targetBox.classList.add('empty__list');
    }
    appendUl.innerHTML = appendHtmlStr;
}
