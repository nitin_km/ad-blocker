let storageKeys = {};
storageKeys = {
    optedAdBlock: "optedAdBlock",
    optedAdTrack: "optedAdTrack",
    optedRiskySites: "optedRiskySites",
    adBlockCount: "adBlockCount",
    adTrackCount: "adTrackCount",
    riskySitesCount: "riskySitesCount",
    whiteListedDomain: "whiteListedDomain",
    nonIntrusiveAdsOptin: "nonIntrusiveAdsOptin",
    setTabActive: "setTabActive",
};

let msgPassingKeys = {};
msgPassingKeys = {
    adBlockStatus: 'blockStatus',
    adTrackStatus: 'trackStatus',
    nonIntrusiveAdStatus: "nonIntrusiveAdStatus"
}