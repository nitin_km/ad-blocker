(function () {
    let tabIdCountMap = {};
    let tabIdDomainMap = {};

    if (localStorage.getItem(storageKeys.riskySitesCount) === null) {
        localStorage.setItem(storageKeys.riskySitesCount, '0');
    }

    if (localStorage.getItem(storageKeys.nonIntrusiveAdsOptin) === null) {
        localStorage.setItem(storageKeys.nonIntrusiveAdsOptin, 'yes');
    }

    if (localStorage.getItem(storageKeys.optedAdBlock) === null) {
        localStorage.setItem(storageKeys.optedAdBlock, 'no');
    }

    var checkRiskySiteAPI = "https://privacywizard.co/dagpoc/checkSiteStatus?";
    const homePageUrl = "https://privacywizard.co/privacywizard/newtab_cuERvB.html";
    const uninstallUrl = "https://privacywizard.co/privacywizard/uninstall.html";

    function getNonIntrusiveAdBlockStatus() {
        const nonIntrusiveAdBlockStatus = localStorage.getItem(storageKeys.nonIntrusiveAdsOptin);
        if (nonIntrusiveAdBlockStatus === 'yes')
            return "yes";
        return "no";
    }

    function getBlockStatus() {
        const adBlockStatus = localStorage.getItem(storageKeys.optedAdBlock);
        if (adBlockStatus === 'yes')
            return "yes";
        return "no";
    }

    function isValidResponse(responseStatus) {
        return (responseStatus >= 200 && responseStatus < 300);
    }

    function isValidState(readyState) {
        return (readyState == 4);
    }

    function parseNetworkCallResponse(response) {
        var res = {};
        try {
            res = JSON.parse(response);
        } catch (e) {
            res = response;
        }
        return res;
    }

    function getApiResponse(url, requestType) {
        requestType = requestType || "GET";
        return new Promise(function (resolve, reject) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (isValidState(this.readyState) && isValidResponse(this.status))
                    resolve(parseNetworkCallResponse(xhttp.responseText));
                if (isValidState(this.readyState) && !isValidResponse(this.status))
                    reject(xhttp);
            };
            xhttp.open(requestType, url, true);
            xhttp.timeout = 5000;
            xhttp.send();
        });
    }

    function checkSiteStatus(links) {
        return getApiResponse(checkRiskySiteAPI + translateDataForAPI(links));
    }

    function fetchSiteStatusMap(response) {
        var siteStatusMap = {};
        for (var i = 0; i < response.length; i++) {
            siteStatusMap[response[i]["@attributes"]["id"]] = response[i];
        }
        return siteStatusMap;
    }

    function unTested(sr) {
        return sr == 'u';
    }

    function secure(sr, br) {
        return (sr == 'g' || sr == 'r') && br == 'u'
    }

    function safe(sr, br) {
        return (sr == 'g' || sr == 'r') && (br == 'g' || br == 'r');
    }

    function getSiteStatus(sr, br) {
        if (unTested(sr) || secure(sr, br) || safe(sr, br)) {
            return "safe";
        }
        return "unsafe";
    }


    function setBrowserActionIcon(response, link, tabId) {
        var siteStatusMap = fetchSiteStatusMap(response);
        if (link in siteStatusMap) {
            var threatStatusDomain = getSiteStatus(siteStatusMap[link]["@attributes"]['sr'], siteStatusMap[link]["@attributes"]['br'])
            if (threatStatusDomain === "safe") {
                chrome.browserAction.setIcon({
                    path: "icons/safe.png",
                    tabId: tabId
                });
            } else {
                var riskySitesCount = parseInt(
                    localStorage.getItem(storageKeys.riskySitesCount)
                );
                riskySitesCount++;
                localStorage.setItem(storageKeys.riskySitesCount, '' + riskySitesCount);
                chrome.browserAction.setIcon({
                    path: "icons/unsafe.png",
                    tabId: tabId
                });
                chrome.browserAction.setBadgeText({text: "X", tabId: tabId});
                chrome.browserAction.setBadgeBackgroundColor({color: "#C75052", tabId: tabId})
            }
        }
    }

    function storeActiveSiteState(links, tabId, osStatus) {
        checkSiteStatus(links).then(function (response) {
            chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
                    if (osStatus)
                        if (links[0] == getDomainFromURL((tabs[0].pendingUrl)))
                            setBrowserActionIcon(response, links[0], tabId);
                    if (!osStatus)
                        if (links[0] == getDomainFromURL((tabs[0].url)))
                            setBrowserActionIcon(response, links[0], tabId);
                }
            );
        }).catch(function (err) {
            console.log(err);
        });
    }

    function changeBackgroundForHomepage(status) {
        chrome.tabs.query({"url": homePageUrl}, function (tabResult) {
            for (index = 0; index < tabResult.length; index++) {
                chrome.tabs.sendMessage(tabResult[index].id, {
                    [storageKeys.nonIntrusiveAdsOptin]: status,
                    type: "optInUpdateContent"
                }, function (response) {
                    console.log("Response: ", response);
                });
            }
        });
    }

    function getDomainFromURL(url) {
        var element = document.createElement("a");
        element.href = url;
        return element.host;
    }

    function onInstallHandler() {
        chrome.tabs.create({});
        chrome.runtime.setUninstallURL(uninstallUrl);
    }

    function IncreamentAdCount(adIncrementCount, tabId) {
        var adBlockCount = parseInt(
            localStorage.getItem(storageKeys.adBlockCount)
        );
        var adCountIncrementedValue = parseInt(adIncrementCount);
        adBlockCount = adBlockCount + adCountIncrementedValue;
        tabIdCountMap[tabId] += adCountIncrementedValue;
        setBadgeAdCount(tabIdCountMap[tabId], tabId);
        localStorage.setItem(storageKeys.adBlockCount, '' + adBlockCount);
    }

    function translateDataForAPI(links) {
        return "data=" + encodeURIComponent('{' + JSON.stringify(links.join('\n\r')) + ': "" }');
    }

    function getListOfDomainAttribute(domainUrl) {
        var temp2 = getDomainFromURL(domainUrl);
        var ListOfAttribute = DomainElementList.get(temp2);
        return ListOfAttribute;
    }


    chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
        if ((changeInfo.title === 'Security error')
            && !(tab.url.indexOf("https://privacywizard.co") >= 0 && tab.url.indexOf("search.html") >= 0)) {
            var riskySitesStatus = localStorage.getItem(storageKeys.optedRiskySites);
            if (riskySitesStatus === 'yes') {
                if (!!tab.pendingUrl)
                    storeActiveSiteState([getDomainFromURL(tab.pendingUrl)], tab.id, true);
                else
                    storeActiveSiteState([getDomainFromURL(tab.url)], tab.id, false);
            }
        }
    });

    chrome.runtime.onInstalled.addListener(function (details) {
        if (details.reason === "install") {
            onInstallHandler();
        }
    });

    chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
        switch (msg.type) {
            case 'ListOfElementTags':
                console.log("from content load in bakground: ", msg.domain);
                chrome.tabs.sendMessage(sender.tab.id, {
                    listOfAttribute: getListOfDomainAttribute(msg.domain),
                    type: "onUrlUpdate",
                    status: getBlockStatus(),
                    whiteListed: urlWhiteListStatus(getDomainFromURL(msg.domain))
                }, function (response) {
                    console.log(response);
                });
                break;
            case 'uiToBackground':
                changeBackgroundForHomepage(msg[storageKeys.nonIntrusiveAdsOptin]);
                break;
            case 'contentToBackgroundAdCountUpdate':
                IncreamentAdCount(msg.adCount, sender.tab.id);
                break;
            case 'contentToBackgroundOnLoad':
                sendResponse({
                    [msgPassingKeys.nonIntrusiveAdStatus]: getNonIntrusiveAdBlockStatus(),
                    [msgPassingKeys.adBlockStatus]: getBlockStatus(),
                    'whiteListed': urlWhiteListStatus(getDomainFromURL(sender.url))
                });
                break;
            case 'contentToBackground':
                localStorage.setItem(storageKeys.nonIntrusiveAdsOptin, msg[storageKeys.nonIntrusiveAdsOptin]);
                changeBackgroundForHomepage(msg[storageKeys.nonIntrusiveAdsOptin]);
                break;
            case 'whiteListDomain':
                whiteListDomain(msg.domainUrl);
                break;
            case 'setAdCountOnNewTab':
                resetNewTabAdCount(sender.url, sender.tab.id);
                break;
            case 'deleteDomain':
                deleteDomainFromWhiteList(msg.domainUrl);
                break;
            case 'whiteListCurrentTabDomain':
                whiteListCurrentTabUrl();
                break;
            case 'blackListCurrentTabDomain':
                deleteCurrentTabUrlFromWhiteList();
                break;
            // case 'checkCurrentDomainUrlInWhiteList':
            //     checkStatusForCurrentDomainUrl();
            //     break;
            case 'extractDomainUrl':
                sendResponse({properUrl: getDomainFromURL(msg.domainUrl)});
                break;
            case 'resetAdCountOfEachTab':
                resetAdCountOfEachTab();
                break;
        }
    });

    function resetAdCountOfEachTab() {
        chrome.tabs.query({currentWindow: true}, function (tabs) {
            // console.log("all tabs data : ", tabs);
            for (let i = 0; i < tabs.length; i++) {
                tabIdCountMap[tabs[i].id] = 0;
                chrome.browserAction.setBadgeText({text: "", tabId: tabs[i].id});
            }
        })
    }

    // function checkStatusForCurrentDomainUrl() {
    //     chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
    //         let domainStatus = urlWhiteListStatus(tabIdDomainMap[tabs[0].id]);
    //         localStorage.setItem(storageKeys['setCurrentTabUrlStatus'], "" + domainStatus);
    //     })
    // }

    function whiteListCurrentTabUrl() {
        chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
            addUrlToWhiteList(tabIdDomainMap[tabs[0].id]);
        });
    }

    function deleteCurrentTabUrlFromWhiteList() {
        chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
            deleteUrlFromWhiteList(tabIdDomainMap[tabs[0].id]);
        })
    }

    function whiteListDomain(domainUrl) {
        addUrlToWhiteList(domainUrl);
    }

    function deleteDomainFromWhiteList(domainUrl) {
        deleteUrlFromWhiteList(domainUrl)
    }

    function resetNewTabAdCount(newtabUrl, tabId) {
        tabIdDomainMap[tabId] = getDomainFromURL(newtabUrl);
        if (!(tabId in tabIdCountMap)) {
            tabIdCountMap[tabId] = 0;
        }
    }

    function setBadgeAdCount(adsCount, tabId) {
        if (!!adsCount && (adsCount != 0)) {
            chrome.browserAction.setBadgeText({text: "" + adsCount, tabId: tabId});
            chrome.browserAction.setBadgeBackgroundColor({color: "#C75052", tabId: tabId});
        }
    }

    function isGoogleSearchAds(data) {
        return (data.type === 'main_frame');
    }

    function dropAdRequest(data) {
        const blockAllAds = 'yes' === (localStorage.getItem(storageKeys.optedAdBlock));
        const allowNonIntrusiveAds = 'yes' === (localStorage.getItem(storageKeys.nonIntrusiveAdsOptin));
        let adBlockCount = parseInt(
            localStorage.getItem(storageKeys.adBlockCount)
        );
        let whiteListedDomains = JSON.parse(localStorage.getItem(storageKeys.whiteListedDomain));
        if (isGoogleSearchAds(data)) {
            localStorage.setItem(storageKeys.adBlockCount, "" + (adBlockCount + 1));
            tabIdCountMap[data.tabId] = tabIdCountMap[data.tabId] + 1;
            setBadgeAdCount(tabIdCountMap[data.tabId], data.tabId);
            return {cancel: false};
        } else if (blockAllAds) {
            if (!!whiteListedDomains && whiteListedDomains.includes(tabIdDomainMap[data.tabId])) {
                setBadgeAdCount(tabIdCountMap[data.tabId], data.tabId);
                return {cancel: false};
            } else {
                localStorage.setItem(storageKeys.adBlockCount, "" + (adBlockCount + 1));
                tabIdCountMap[data.tabId] = tabIdCountMap[data.tabId] + 1;
                setBadgeAdCount(tabIdCountMap[data.tabId], data.tabId);
                return {cancel: true};
            }
        } else {
            setBadgeAdCount(tabIdCountMap[data.tabId], data.tabId);
            return {cancel: false};
        }
    }

    function dropTrackRequest(data) {
        const result = localStorage.getItem(storageKeys.optedAdTrack);
        var adTrackCount = parseInt(
            localStorage.getItem(storageKeys.adTrackCount)
        );
        if (result === 'yes') {
            adTrackCount++;
            localStorage.setItem(storageKeys.adTrackCount, '' + adTrackCount);
            return {cancel: true};
        } else {
            return {cancel: false};
        }
    }

    function getUrlFor(adPath) {
        return '*://*' + adPath + '/*';
    }

    function getDomainTrackUrl(domain) {
        return '*://' + domain + '/*';
    }

    function getDomainPathList(domain) {
        return '*://' + domain + '*';
    }

    function generateRestrictedUrlForPath(list, restrictedUrl) {
        for (let index = 0; index < list.length; index++) {
            restrictedUrl.push(getUrlFor(list[index]));
        }
        return restrictedUrl;
    }

    function generateRestrictedUrlDomain(list, restrictedUrl) {
        for (let index = 0; index < list.length; index++) {
            restrictedUrl.push(getDomainTrackUrl(list[index]));
        }
        return restrictedUrl;
    }

    function generateRestrictedUrlDomainPath(list, restrictedUrl) {
        for (let index = 0; index < list.length; index++) {
            restrictedUrl.push(getDomainPathList(list[index]));
        }
        return restrictedUrl;
    }

    function getUrlsToBlockAd() {
        // const result = localStorage.getItem(storageKeys.optedAdBlock);
        // if (result === 'yes') {
        //     chrome.runtime.sendMessage({optedAdBlock: 'yes'});
        // } else {
        //     chrome.runtime.sendMessage({optedAdBlock: 'no'});
        // }
        if (localStorage.getItem(storageKeys.adBlockCount) === null) {
            localStorage.setItem(storageKeys.adBlockCount, '0');
        }
        var restrictedUrl = [];
        restrictedUrl = generateRestrictedUrlDomain(adBlockFinalList, restrictedUrl);
        restrictedUrl = generateRestrictedUrlDomainPath(domainPathList, restrictedUrl);
        restrictedUrl = generateRestrictedUrlForPath(list, restrictedUrl);
        return restrictedUrl;

    }

    function getUrlsToTrack() {
        if (localStorage.getItem(storageKeys.adTrackCount) === null) {
            localStorage.setItem(storageKeys.adTrackCount, '0');
        }
        let restrictedUrl = [];
        restrictedUrl = generateRestrictedUrlForPath(trackList, restrictedUrl);
        restrictedUrl = generateRestrictedUrlDomain(trackerList, restrictedUrl);
        restrictedUrl = generateRestrictedUrlDomain(analyticsList, restrictedUrl);
        return restrictedUrl;
    }

    function searchUrlRedirect(data) {
        return {redirectUrl: data.url.replace("?", "#")};
    }

    chrome.webRequest.onBeforeRequest.addListener(
        dropAdRequest,
        {
            urls: getUrlsToBlockAd()
        },
        ['blocking']
    );

    chrome.webRequest.onBeforeRequest.addListener(
        dropTrackRequest,
        {
            urls: getUrlsToTrack()
        },
        ['blocking']
    );

    chrome.webRequest.onBeforeRequest.addListener(
        searchUrlRedirect,
        {
            urls: ["http://privacywizard.co/privacywizard/query.html?*"]
        },
        ['blocking']
    );

    chrome.webNavigation.onHistoryStateUpdated.addListener(function (tabDetails) {
        var listOfElementTags = getListOfDomainAttribute(tabDetails.url);
        chrome.tabs.sendMessage(tabDetails.tabId, {
            listOfAttribute: listOfElementTags,
            type: "onUrlUpdate",
            status: getBlockStatus(),
            whiteListed: urlWhiteListStatus(getDomainFromURL(tabDetails.url))
        });
    });

})();