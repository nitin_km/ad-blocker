let msgPassingKeys = {};
msgPassingKeys = {
    adBlockStatus: 'blockStatus',
    adTrackStatus: 'trackStatus',
    riskySitesStatus: 'riskySitesStatus',
    nonIntrusiveAdStatus: "nonIntrusiveAdStatus"
}

function blockAddHtml() {
    let adCountUpdatedValue = 0;

    // const googleAdElements = document.querySelectorAll("[aria-label='Ads']");
    // for (let i = 0; i < googleAdElements.length; i++)
    //     googleAdElements[i].innerHTML = "";

    const askAdElements = document.getElementsByClassName("adBlock");
    for (let i = 0; i < askAdElements.length; i++) {
        console.error("Ads are Blocked", askAdElements[i]);
        askAdElements[i].innerHTML = "";
    }
    adCountUpdatedValue += askAdElements.length;

    // var bingAdElements = document.getElementsByClassName("b_ad");
    // for (let i = 0; i < bingAdElements.length; i++) {
    //     console.error("Ads are Blocked",bingAdElements[i]);
    //     bingAdElements[i].innerHTML = "";
    // }
    // adCountUpdatedValue += bingAdElements.length;

    // bingAdElements = document.getElementsByClassName("sb_add");
    // for (let i = 0; i < bingAdElements.length; i++) {
    //     console.error("Ads are Blocked",bingAdElements[i]);
    //     bingAdElements[i].innerHTML = "";
    // }
    // adCountUpdatedValue += bingAdElements.length;

    let yahooAdElements = document.getElementsByClassName("searchCenterTopAds");
    for (let i = 0; i < yahooAdElements.length; i++) {
        console.error("Ads are Blocked", yahooAdElements[i]);
        yahooAdElements[i].innerHTML = "";
    }
    adCountUpdatedValue += yahooAdElements.length;

    yahooAdElements = document.getElementsByClassName("searchRightBottomAds");
    for (let i = 0; i < yahooAdElements.length; i++) {
        console.error("Ads are Blocked", yahooAdElements[i]);
        yahooAdElements[i].innerHTML = "";
    }
    adCountUpdatedValue += yahooAdElements.length;

    chrome.runtime.sendMessage({
        type: "contentToBackgroundAdCountUpdate",
        adCount: adCountUpdatedValue
    }, function (response) {
        console.log(response);
    });
}

document.addEventListener('newtabNonIntrusiveAdBlockStatus', function (e) {
    // notifyExtension(e.detail.status);
    chrome.runtime.sendMessage({nonIntrusiveAdsOptin: e.detail.status, type: "contentToBackground"});
});

chrome.runtime.sendMessage({type: "ListOfElementTags", domain: window.location.href}, function (response) {
    console.log(response);
});


function hideDocumentElement(listOfAttribute, Status) {
    var attributeList = listOfAttribute;
    var adCountUpdatedValue = 0;
    var status = "" + Status;
    if (status == "yes" && !!attributeList) {
        for (var i = 0; i < attributeList.length; i++) {
            var listOfElement = document.getElementById(attributeList[i]);
            if (listOfElement != null) {
                listOfElement.remove();
            }
            var listOfClassElement = document.getElementsByClassName(attributeList[i]);
            if (listOfClassElement != null) {
                while (listOfClassElement.length > 0) {
                    listOfClassElement[0].remove();
                    adCountUpdatedValue++;
                    console.error("Ads are Blocked");
                }
            }
        }
    }
    chrome.runtime.sendMessage({
        type: "contentToBackgroundAdCountUpdate",
        adCount: adCountUpdatedValue
    }, function (response) {
        console.log(response);
    });
}

setInterval(function () {
    var elements = document.getElementsByClassName("ytp-ad-overlay-container");
    if (elements) {
        for (var i = 0; i < elements.length; i++) {
            elements[i].remove();
        }
    }
}, 500);

chrome.runtime.sendMessage({type: "contentToBackgroundOnLoad"}, function (response) {
    var allowNonIntrusiveAds = 'yes' === ("" + response[msgPassingKeys.nonIntrusiveAdStatus]);
    var blockAllAds = 'yes' === ("" + response[msgPassingKeys.adBlockStatus]);
    var whiteListed = response['whiteListed'];

    var blockFordomain = (blockAllAds && !whiteListed);
    if (blockFordomain && !allowNonIntrusiveAds) {
        blockAddHtml();
    }
    document.dispatchEvent(new CustomEvent("nonIntrusiveAdsStatus", {detail: {status: response[msgPassingKeys.nonIntrusiveAdStatus]}}));
});


// this part for youtube ad block because of new element gets added to the website
chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
    switch (message.type) {
        case 'onUrlUpdate':
            if (!message.whiteListed)
                for (var i = 0; i < 3; i++) {
                    setTimeout(function () {
                        hideDocumentElement(message.listOfAttribute, message.status);
                    }, i * 500);
                }
            break;
        case 'optInUpdateContent':
            var status = message.nonIntrusiveAdsOptin;
            document.dispatchEvent(new CustomEvent("nonIntrusiveAdsStatus", {detail: {status: status}}));
            break;
        default:
            console.log("Calling default");
    }
});



