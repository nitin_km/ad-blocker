function addUrlToWhiteList(url) {
    let whiteListedDomain = JSON.parse(localStorage.getItem(storageKeys.whiteListedDomain));
    if (whiteListedDomain == null) {
        whiteListedDomain = [];
    }
    const index = whiteListedDomain.indexOf(url);
    if (!(index > -1)) {
        whiteListedDomain.push(url);
    }
    if (!!url)
        localStorage.setItem(storageKeys.whiteListedDomain, JSON.stringify(whiteListedDomain));
}

function deleteUrlFromWhiteList(url) {
    let whiteListedDomain = JSON.parse(localStorage.getItem(storageKeys.whiteListedDomain));
    if (whiteListedDomain == null) {
        whiteListedDomain = [];
    }
    console.log("url in helper: ", url);
    const index = whiteListedDomain.indexOf(url);
    if (index > -1) {
        whiteListedDomain.splice(index, 1);
    }
    localStorage.setItem(storageKeys.whiteListedDomain, JSON.stringify(whiteListedDomain));
}

function urlWhiteListStatus(url) {
    let whiteListedDomain = [];
    try {
        whiteListedDomain = JSON.parse(localStorage.getItem(storageKeys.whiteListedDomain)) || [];
    } catch (e) {
        console.log(e);
    }
    return whiteListedDomain.indexOf(url) > -1;
}